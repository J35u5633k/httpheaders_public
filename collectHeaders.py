#python3

"""
Read in list of domains, collect all HTTP response headers.

See associated blog post for more information: [LINK]

Top 1 million domains: http://s3.amazonaws.com/alexa-static/top-1m.csv.zip. Could automate the download and extraction like this: https://gist.github.com/chilts/7229605

---------------------

cat collectHeaders.log |grep singleHeader | awk -F ":" '{print $1}' |sort | uniq -c |sort -n > headerCountPerDomain.txt
795258 successful domains collected

cat collectHeaders.log |grep singleHeader | awk -F ":" '{print $4}' | sort -f | uniq -ic | sort -n > uniqueHeaders.txt

cat collectHeaders.log | grep -Po "(?<=[^0-9])[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(?=[^0-9])" | grep -v "123.123.123.123" |sort | uniq -c |sort -n > parsedIPs_unique.txt

(10|192|172)\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}

cat collectHeaders.log | grep singleHeader | grep -i hostname | grep -iPv "X\-Pantheon\-Styx\-Hostname|nexcess\.net" | awk -F ":" '{print $5}' |sort | uniq -ci |sort -n > parsedHostnames_unique.txt


cat uniqueHeaders.txt | grep -i xss | grep -iv "X-XSS-Protection" > possibleHeaderTypo_x-xss-protection.txt

cat collectHeaders.log |grep -i "x-xss-protection" |sed -E 's/^[^:]+:[^:]+:[^:]+:[^:]+://g' | grep -vP "Shopify|MissingHeaderBodySeparatorDefect" |sort -f|uniq -ci|sort -n > headerValues_x-xss-protection.txt


cat uniqueHeaders.txt | grep -i frame | grep -iv "X-frame-options" > possibleHeaderTypo_x-frame-options.txt

cat collectHeaders.log |grep -i "x-frame-options" |sed -E 's/^[^:]+:[^:]+:[^:]+:[^:]+://g' | grep -vP "MissingHeaderBodySeparatorDefect" |sort -f|uniq -ci|sort -n > headerValues_x-frame-options.txt


cat collectHeaders.log |grep finalHSTS | sed -E 's/^[^:]+:[^:]+:finalHSTS://g' | sort -f | uniq -ci| sort -n > headerValues_hsts.txt


cat uniqueHeaders.txt | grep -i Referrer | grep -iv "Referrer-Policy" > possibleHeaderTypo_referrer-policy.txt

cat collectHeaders.log |grep -i "Referrer-Policy" |sed -E 's/^[^:]+:[^:]+:[^:]+:[^:]+://g' | grep -vP "MissingHeaderBodySeparatorDefect" |sort -f|uniq -ci|sort -n > headerValues_referrer-policy.txt


cat uniqueHeaders.txt | grep -i policy | grep -iv "content-security-policy" > possibleHeaderTypo_content-security-policy.txt

cat collectHeaders.log |grep -i "content-security-policy" |sed -E 's/^[^:]+:[^:]+:[^:]+:[^:]+://g' | grep -vP "MissingHeaderBodySeparatorDefect" |sort -f|uniq -ci|sort -n > headerValues_content-security-policy.txt


etc.


"""

#imports
import requests
import argparse
import urllib3
import threading
import time
import sys
import logging
from time import sleep

# global consts
UA = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36"
MAX_THREADS = 25

LOG_FILE = sys.argv[0] + "_" + time.strftime('%Y-%m-%d_%H-%M-%S') + ".log"
#logging.basicConfig(format='%(asctime)s [%(levelname)s][%(threadName)s]  %(message)s', filename=LOG_FILE, level=logging.INFO)
logging.basicConfig(format='%(threadName)s:%(levelname)s:%(message)s', filename=LOG_FILE, level=logging.INFO)


class grabHeaders(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.currentDomain = name

    def run(self):
        HSTSheaderFound = None
        #logging.info("\ncurrentDomain: " + self.currentDomain)

        try:
            x = requests.get("http://" + self.currentDomain, headers={'User-Agent': UA}, allow_redirects=True, timeout=10, verify=False)
        except:
            logging.error("error connecting")
            return

        logging.info("finalURL:%s" % x.url)
        if len(x.headers) > 1:
            for header in x.headers:
                logging.info("singleHeader:%s:%s" % (header, x.headers[header]))
                if "strict-transport-security" == header.lower():
                    HSTSheaderFound = header
                    break

        if HSTSheaderFound:
            logging.info("finalHSTS:%s" % (x.headers[HSTSheaderFound]))
        else:
            logging.info("finalHSTS:none")


def main():
    parser = argparse.ArgumentParser(description="Read in list of domains, check for HSTS header.")
    parser.add_argument("-f", "--filePath", required=True, help="list of domains, one each line, no spaces, characters, headers, etc.")
    parser.add_argument("-v", "--verbose", help='be verbose', action='store_true')
    args = parser.parse_args()

    if args.verbose:
        logging.getLogger("history").setLevel(logging.DEBUG)
    else:
        logging.getLogger("history").setLevel(logging.INFO)


    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    myThreads = []
    buffer = []

    try:
        with open(args.filePath, "r") as f:
            for line in f.readlines():
                currentDomain = line.strip()
                if not len(currentDomain) > 1:
                    continue
                objx = grabHeaders(currentDomain)
                objx._name = currentDomain
                objx.start()
                myThreads.append(objx)
                print("thread count %s" % threading.activeCount())
                while(threading.activeCount() > MAX_THREADS):
                    sleep(0.01)
                sleep(0.001)
                
    except IOError:
        print("Error reading file '%s'" % args.filePath)


# Start execution
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Keyboard interrupted detected')
        sys.exit(0)
